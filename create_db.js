var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password"
});
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  /*Create a database named "test":*/
  con.query("CREATE DATABASE test", function (err, result) {
    if (err) throw err;
    console.log("Database created");
  });
});

