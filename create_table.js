var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "test"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  /*Create a table named "customers":*/
  var sql = "CREATE TABLE users ( id int(11) NOT NULL auto_increment, name varchar(100) NOT NULL, age int(3) NOT NULL, email varchar(100) NOT NULL, PRIMARY KEY (id) )";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });
});

